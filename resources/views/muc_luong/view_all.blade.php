@extends('layer.master')
@section('body')
<a href="{{ route('muc_luong.view_insert') }}"> Thêm</a>
<table border="1" width="100%">
	<tr>
		<th>
			Mã
		</th>
		<th>
			Số Tiền Trên Một Giờ
		</th>
		<th>
			Sửa
		</th>
	</tr>
	@foreach($array as $each)
	<tr>
		<td>
			{{$each->ma_muc_luong}}
		</td>
		<td>
			{{$each->so_tien_tren_1_gio}}
		</td>
		<td>
				<a href="{{ route('muc_luong.view_update',['ma_muc_luong' => $each->ma_muc_luong]) }}">
					<button>Sửa</button>
				</a>
			</td>
	</tr>

	@endforeach
</table>
@endsection