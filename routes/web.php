<?php
Route::get('test','Controller@test');

Route::group(['middleware' => 'CheckKeToan'],function(){

	Route::group(['prefix' => 'muc_luong', 'as' => 'muc_luong.'],function(){
		Route::get('','LuongController@view_all')->name('view_all');
		Route::get('view_insert','LuongController@view_insert')->name('view_insert');
		Route::post('process_insert','LuongController@process_insert')->name('process_insert');


		Route::get('view_update/{ma_muc_luong}','LuongController@view_update')->name('view_update');
		Route::post('process_update/{ma_muc_luong}','LuongController@process_update')->name('process_update');
	});
	?>

	<?php

	Route::group(['prefix' => 'ke_toan', 'as' => 'ke_toan.'],function(){
		Route::get('','Ke_toanController@view_all')->name('view_all_ke_toan');
		Route::get('view_insert','Ke_toanController@view_insert')->name('view_insert_ke_toan');
		Route::post('process_insert','Ke_toanController@process_insert')->name('process_insert');

		Route::get('view_update/{ma_ke_toan}','Ke_toanController@view_update')->name('view_update_ke_toan');
		Route::post('process_update/{ma_ke_toan}','Ke_toanController@process_update')->name('process_update');
	});

});

Route::group(['middleware' => 'CheckGiaoVien'],function(){

	Route::group(['prefix' => 'giao_vien', 'as' => 'giao_vien.'],function(){
		Route::get('','GiaoVienController@view_all')->name('view_all');
		Route::get('view_insert','GiaoVienController@view_insert')->name('view_insert');
		Route::post('process_insert','GiaoVienController@process_insert')->name('process_insert');


		Route::get('view_update/{ma_giao_vien}','GiaoVienController@view_update')->name('view_update');
		Route::post('process_update/{ma_giao_vien}','GiaoVienController@process_update')->name('process_update');
	});	
});

Route::get('login','Controller@login')->name('view_login');
Route::post('process_login','Controller@process_login')->name('process_login');

Route::get('logout','Controller@logout')->name('logout');
?>