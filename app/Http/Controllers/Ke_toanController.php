<?php
namespace App\Http\Controllers;

use App\Model\Ke_toanModel;
use Illuminate\Http\Request;
class Ke_toanController
{
	function view_all(){
		$array = Ke_toanModel::get_all();
		return view('view_all_ke_toan',compact('array'));
	}
	function view_insert(){
		return view('view_insert_ke_toan');
	}
	function process_insert(Request $rq){
		$ke_toan = new Ke_toanModel();
		$ke_toan->ten_ke_toan = $rq ->get('ten_ke_toan');
		$ke_toan->gioi_tinh = $rq ->get('gioi_tinh');
		$ke_toan->ngay_sinh = $rq ->get('ngay_sinh');
		$ke_toan->email = $rq ->get('email');
		$ke_toan->mat_khau = $rq ->get('mat_khau');
		$ke_toan->dia_chi = $rq ->get('dia_chi');
		$ke_toan->insert();
		return redirect()->route('view_all_ke_toan');
	} 
	function view_update($ma_ke_toan)
	{
		$each = Ke_toanModel::get_one($ma_ke_toan);

		return view('view_update_ke_toan',compact('each'));
	}
	function process_update($ma_ke_toan, Request $rq)
	{
		$ke_toan = new Ke_toanModel();
		$ke_toan->ma_ke_toan = $ma_ke_toan;
		$ke_toan->ten_ke_toan = $rq ->get('ten_ke_toan');
		$ke_toan->gioi_tinh = $rq ->get('gioi_tinh');
		$ke_toan->ngay_sinh = $rq ->get('ngay_sinh');
		$ke_toan->email = $rq ->get('email');
		$ke_toan->mat_khau = $rq ->get('mat_khau');
		$ke_toan->dia_chi = $rq ->get('dia_chi');


		$ke_toan->update();

		return redirect()->route("ke_toan.view_all_ke_toan");
	}

}

