<?php

namespace App\Model;

use DB;

class GiaoVienModel
{
	public $ma_giao_vien;
	public $ten_giao_vien;
	public $gioi_tinh;
	public $ngay_sinh;
	public $email;
	public $mat_khau;
	public $dia_chi;
	static function get_all(){
		$array = DB::select('select * from giao_vien');
		return $array;
	}
	public function insert(){
		DB::insert("insert into giao_vien(ten_giao_vien, gioi_tinh, ngay_sinh, email, mat_khau, dia_chi) values (?,?,?,?,?,?)",[
			$this->ten_giao_vien,
			$this->gioi_tinh,
			$this->ngay_sinh,
			$this->email,
			$this->mat_khau,
			$this->dia_chi

		]);
	}

			public function get_login(){
		$array = DB::select('select * from giao_vien where email = ? and mat_khau = ?',[
			$this->email,
			$this->mat_khau
		]);
		return $array;
	}
	static function get_one($ma_giao_vien){
		$array = DB::select('select * from giao_vien where ma_giao_vien = ?',[
			$ma_giao_vien
		]);
		return $array[0];
	}
	public function update()
	{
		DB::update("update giao_vien set
		 ten_giao_vien = ?,
		 gioi_tinh = ?,
		 ngay_sinh = ?,
		 email = ?,
		 mat_khau = ?
		 where ma_giao_vien = ?",[
			$this->ten_giao_vien,
			$this->gioi_tinh,
			$this->ngay_sinh,
			$this->email,
			$this->mat_khau,
			$this->ma_giao_vien
		]);
	}
}