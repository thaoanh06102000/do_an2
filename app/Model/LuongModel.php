<?php
namespace App\Model;
use DB;
class LuongModel
{
	public $ma_muc_luong;
	public $so_tien_tren_1_gio;
	static function get_all(){
		$array = DB::select("select * from muc_luong");
		return $array;
	}
	public function insert(){
		DB::insert("insert into muc_luong(so_tien_tren_1_gio) values(?)",[
			$this->so_tien_tren_1_gio
		]);
	}
	static function get_one($ma_muc_luong){
		$array = DB::select('select * from muc_luong where ma_muc_luong = ?',[
			$ma_muc_luong
		]);
		return $array[0];
	}
	public function update()
	{
		DB::update("update muc_luong set
		 so_tien_tren_1_gio = ?
		 where ma_muc_luong = ?",[
			$this->so_tien_tren_1_gio,
			$this->ma_muc_luong
		]);
	}
}